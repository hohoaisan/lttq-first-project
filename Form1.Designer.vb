﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.Btn_OK = New System.Windows.Forms.Button()
        Me.Btn_Apply = New System.Windows.Forms.Button()
        Me.Lb_MaSinhVien = New System.Windows.Forms.Label()
        Me.Lb_MatKhau = New System.Windows.Forms.Label()
        Me.Tb_MaSinhVien = New System.Windows.Forms.TextBox()
        Me.Tb_MatKhau = New System.Windows.Forms.TextBox()
        Me.Btn_Xoa = New System.Windows.Forms.Button()
        Me.Btn_Login = New System.Windows.Forms.Button()
        Me.Llb_DaoTao = New System.Windows.Forms.LinkLabel()
        Me.Rtb_Nhap = New System.Windows.Forms.RichTextBox()
        Me.SuspendLayout()
        '
        'Btn_OK
        '
        Me.Btn_OK.Location = New System.Drawing.Point(568, 263)
        Me.Btn_OK.Name = "Btn_OK"
        Me.Btn_OK.Size = New System.Drawing.Size(80, 30)
        Me.Btn_OK.TabIndex = 0
        Me.Btn_OK.Text = "OK"
        Me.Btn_OK.UseVisualStyleBackColor = True
        '
        'Btn_Apply
        '
        Me.Btn_Apply.Location = New System.Drawing.Point(482, 263)
        Me.Btn_Apply.Name = "Btn_Apply"
        Me.Btn_Apply.Size = New System.Drawing.Size(80, 30)
        Me.Btn_Apply.TabIndex = 1
        Me.Btn_Apply.Text = "Apply"
        Me.Btn_Apply.UseVisualStyleBackColor = True
        '
        'Lb_MaSinhVien
        '
        Me.Lb_MaSinhVien.AutoSize = True
        Me.Lb_MaSinhVien.Location = New System.Drawing.Point(52, 86)
        Me.Lb_MaSinhVien.Name = "Lb_MaSinhVien"
        Me.Lb_MaSinhVien.Size = New System.Drawing.Size(69, 13)
        Me.Lb_MaSinhVien.TabIndex = 2
        Me.Lb_MaSinhVien.Text = "Mã Sinh viên"
        '
        'Lb_MatKhau
        '
        Me.Lb_MatKhau.AutoSize = True
        Me.Lb_MatKhau.Location = New System.Drawing.Point(52, 123)
        Me.Lb_MatKhau.Name = "Lb_MatKhau"
        Me.Lb_MatKhau.Size = New System.Drawing.Size(52, 13)
        Me.Lb_MatKhau.TabIndex = 3
        Me.Lb_MatKhau.Text = "Mật khẩu"
        '
        'Tb_MaSinhVien
        '
        Me.Tb_MaSinhVien.Location = New System.Drawing.Point(127, 83)
        Me.Tb_MaSinhVien.MaxLength = 25
        Me.Tb_MaSinhVien.Name = "Tb_MaSinhVien"
        Me.Tb_MaSinhVien.Size = New System.Drawing.Size(124, 20)
        Me.Tb_MaSinhVien.TabIndex = 4
        '
        'Tb_MatKhau
        '
        Me.Tb_MatKhau.Location = New System.Drawing.Point(127, 120)
        Me.Tb_MatKhau.MaxLength = 25
        Me.Tb_MatKhau.Name = "Tb_MatKhau"
        Me.Tb_MatKhau.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.Tb_MatKhau.Size = New System.Drawing.Size(124, 20)
        Me.Tb_MatKhau.TabIndex = 5
        '
        'Btn_Xoa
        '
        Me.Btn_Xoa.Location = New System.Drawing.Point(55, 161)
        Me.Btn_Xoa.Name = "Btn_Xoa"
        Me.Btn_Xoa.Size = New System.Drawing.Size(85, 28)
        Me.Btn_Xoa.TabIndex = 6
        Me.Btn_Xoa.Text = "Xoá"
        Me.Btn_Xoa.UseVisualStyleBackColor = True
        '
        'Btn_Login
        '
        Me.Btn_Login.Location = New System.Drawing.Point(162, 161)
        Me.Btn_Login.Name = "Btn_Login"
        Me.Btn_Login.Size = New System.Drawing.Size(89, 28)
        Me.Btn_Login.TabIndex = 7
        Me.Btn_Login.Text = "Đăng nhập"
        Me.Btn_Login.UseVisualStyleBackColor = True
        '
        'Llb_DaoTao
        '
        Me.Llb_DaoTao.AutoSize = True
        Me.Llb_DaoTao.Location = New System.Drawing.Point(71, 213)
        Me.Llb_DaoTao.Name = "Llb_DaoTao"
        Me.Llb_DaoTao.Size = New System.Drawing.Size(116, 13)
        Me.Llb_DaoTao.TabIndex = 8
        Me.Llb_DaoTao.TabStop = True
        Me.Llb_DaoTao.Text = "Truy cập trang đào tạo"
        '
        'Rtb_Nhap
        '
        Me.Rtb_Nhap.Location = New System.Drawing.Point(331, 12)
        Me.Rtb_Nhap.Name = "Rtb_Nhap"
        Me.Rtb_Nhap.Size = New System.Drawing.Size(317, 245)
        Me.Rtb_Nhap.TabIndex = 9
        Me.Rtb_Nhap.Text = resources.GetString("Rtb_Nhap.Text")
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(659, 302)
        Me.Controls.Add(Me.Rtb_Nhap)
        Me.Controls.Add(Me.Llb_DaoTao)
        Me.Controls.Add(Me.Btn_Login)
        Me.Controls.Add(Me.Btn_Xoa)
        Me.Controls.Add(Me.Tb_MatKhau)
        Me.Controls.Add(Me.Tb_MaSinhVien)
        Me.Controls.Add(Me.Lb_MatKhau)
        Me.Controls.Add(Me.Lb_MaSinhVien)
        Me.Controls.Add(Me.Btn_Apply)
        Me.Controls.Add(Me.Btn_OK)
        Me.Name = "MainForm"
        Me.Text = "Quản lí sinh viên"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Btn_OK As Button
    Friend WithEvents Btn_Apply As Button
    Friend WithEvents Lb_MaSinhVien As Label
    Friend WithEvents Lb_MatKhau As Label
    Friend WithEvents Tb_MaSinhVien As TextBox
    Friend WithEvents Tb_MatKhau As TextBox
    Friend WithEvents Btn_Xoa As Button
    Friend WithEvents Btn_Login As Button
    Friend WithEvents Llb_DaoTao As LinkLabel
    Friend WithEvents Rtb_Nhap As RichTextBox
End Class
